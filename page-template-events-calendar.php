<?php 
/* 
Template Name: Events (Do Not Use For Pages)
*/ 
?>

<?php get_header(); ?>
    <header class='header-internal header-internal-events'>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <?php 
                  if ( is_singular('tribe_events') ) {
                      echo '<h2>Upcoming Events</h2>';
                  } else {
                      echo '<h1>Upcoming Events</h1>';
                  }
              ?>
              
              <p class="subtitle">Get Involved With Land Trust</p>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='page-content<?php echo ( $sidebar_display == true ? ' right-sidebar' : ' full-width' ); ?>'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12'>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
            <?php the_content(); ?>
            <?php endwhile; else: endif; ?>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>