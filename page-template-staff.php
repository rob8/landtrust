<?php 
/* 
Template Name: Board & Staff
*/ 
?>

<?php

    //-----------------------------------------------------
	// Load Widget Inclusions
	//-----------------------------------------------------
	
	$events_display = get_post_meta( $post->ID, 'page_events_widget_display', true );
	$blog_display = get_post_meta( $post->ID, 'page_blog_widget_display', true );
	
	if ( ($events_display == 'on') || ($blog_display == 'on') ) {
    	    $sidebar_display = TRUE;
	} else {
    	    $sidebar_display = FALSE;
	}
    
?>

<?php get_header(); ?>
    <header class='header-internal'<?php echo landtrust_build_page_header_image($post->ID); ?>>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <?php echo landtrust_build_page_header_icon_css($post->ID); ?>
              <h1><?php echo landtrust_build_page_header_title($post->ID); ?></h1>
              <?php echo landtrust_build_page_header_subtitle($post->ID); ?>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='page-content full-width'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-md-10 col-md-offset-1'>
            <?php echo landtrust_build_board_grid($post->ID, 'board'); ?>
            <?php echo landtrust_build_board_grid($post->ID, 'staff'); ?>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>