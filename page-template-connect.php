<?php 
/* 
Template Name: Connect
*/ 
?>



<?php get_header(); ?>
    <header class='header-internal'<?php echo landtrust_build_page_header_image($post->ID); ?>>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <?php echo landtrust_build_page_header_icon_css($post->ID); ?>
              <h1><?php echo landtrust_build_page_header_title($post->ID); ?></h1>
              <?php echo landtrust_build_page_header_subtitle($post->ID); ?>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='page-content full-width'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 text-center'>
            <?php echo landtrust_build_connect_top_intro($post->ID); ?>
            <?php echo landtrust_build_connect_top($post->ID); ?>
            <h2>
              Explore Our Trails
            </h2>
            <div class='row'>
              <div class='col-xs-12 col-md-10 col-md-offset-1'>
                <p class='lead'>
                  The Land Trust offers space for you to relax, to get some exercise, and to connect with nature. Enjoyed by residents and visitors alike, our Nature Preserves and trails are considered some of the very best things about Huntsville!
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php 
        if ( function_exists(snapmap_build_single) ) { ?>
            <?php 
                $mapping_options = get_option( 'cmb2-roadway-segments' );
                $api_key = $mapping_options['apikey'];
                
                $args = array(
                  'post_type'   => 'projects',
                  'meta_query' => array (
                        array (
                            //'relation' => 'OR',
                            'key' => 'project_location', //The field to check.
                            'compare' => 'EXISTS', //Conditional statement used on the value.
                        ),  
                    ),
                );
                
                $projects = get_posts($args);
            ?>
            <div id='connect-map'>
              <div id='map'></div>
              <script>
                function initMap() {
                <!-- / Styles a map in night mode. -->
                var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 34.7440941, lng: -86.6354996},
                zoom: 11,
                gestureHandling: 'none',
                scrollwheel: false,
                mapTypeControl: false,
                streetViewControl: false,
                rotateControl: false,
                styles: [
                {
                'featureType': 'landscape.natural',
                'elementType': 'geometry.fill',
                'stylers': [
                {
                'color': '#cee29e'
                }
                ]
                },
                {
                'featureType': 'poi.business',
                'stylers': [
                {
                'visibility': 'off'
                }
                ]
                },
                {
                'featureType': 'poi.park',
                'elementType': 'geometry.fill',
                'stylers': [
                {
                'color': '#a3d371'
                }
                ]
                },
                {
                'featureType': 'poi.park',
                'elementType': 'labels.text',
                'stylers': [
                {
                'weight': 4.5
                }
                ]
                },
                {
                'featureType': 'poi.school',
                'elementType': 'labels.text',
                'stylers': [
                {
                'visibility': 'off'
                }
                ]
                }
                ]
                });
                var image = {
                url: '<?php bloginfo('template_directory') ?>//images/connect-map-marker.png',
                <!-- / This marker is 20 pixels wide by 32 pixels high. -->
                size: new google.maps.Size(46, 65),
                <!-- / The origin for this image is (0, 0). -->
                origin: new google.maps.Point(0, 0),
                <!-- / The anchor for this image is the base of the flagpole at (0, 32). -->
                anchor: new google.maps.Point(23, 61)
                };
                // Start Loop content
                <?php
                    foreach ( $projects as $post ) :
                        setup_postdata( $post );
                        $location_data = get_post_meta($post->ID, 'project_location', true); ?>
                        var marker = new google.maps.Marker({
                        position: {lat: <?php echo $location_data['lat']; ?>, lng: <?php echo $location_data['lng']; ?>},
                        map: map,
                        icon: image,
                        title: '<?php the_title(); ?>'
                        });
                        google.maps.event.addListener(marker, 'click', function () {
                        window.location.href = '<?php the_permalink(); ?>';
                        });
                    <?php
                    endforeach; 
                    wp_reset_postdata();    
                ?>
                
                // End loop content
                }
              </script>
              <script async defer src='https://maps.googleapis.com/maps/api/js?key=<?php echo $api_key; ?>&amp;callback=initMap'></script>
              <div class='connect-map-overlay'>
                <div class='top'>
                  <h2>Trailheads</h2>
                </div>
                <div class='middle'>
                  Click on a map marker or property name in the list below to view more information and view a map.
                </div>
                <div class='location-list-container'>
                  <ul class='location-list'>
                    <?php
                        foreach ( $projects as $post ) :
                            setup_postdata( $post ); ?>
                            <li>
                              <a href='<?php the_permalink(); ?>'>
                                <?php the_title(); ?>
                              </a>
                            </li>
                        <?php
                        endforeach; 
                        wp_reset_postdata();    
                    ?>
                  </ul>
                </div>
              </div>
            </div>    
        <?php }
    ?>
    
    <div class='connect-map-cta'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 text-center'>
            <img src='<?php bloginfo('template_directory') ?>/images/connect-map-cta-image.png'>
            <span>
              <strong>
                Get maps on your mobile phone
              </strong>
              with our Map App!
            </span>
            <a class='btn btn-primary btn-lg' href='#'>
              Learn More
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id='connect-outro'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-md-8 social-grid'>
            <h2>
              Connect on Social Media
            </h2>
            <div class='row'>
              <div class='col-xs-12 col-sm-6 social-grid-item'>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-facebook fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='social-grid-item-title'>
                    Facebook
                  </span>
                </a>
              </div>
              <div class='col-xs-12 col-sm-6 social-grid-item'>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-twitter fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='social-grid-item-title'>
                    Twitter
                  </span>
                </a>
              </div>
              <div class='col-xs-12 col-sm-6 social-grid-item'>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-instagram fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='social-grid-item-title'>
                    Instagram
                  </span>
                </a>
              </div>
              <div class='col-xs-12 col-sm-6 social-grid-item'>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-youtube-play fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='social-grid-item-title'>
                    YouTube
                  </span>
                </a>
              </div>
            </div>
          </div>
          <div class='col-xs-12 col-md-4 enewsletter-form'>
            <h2>
              Stay Up To Date!
            </h2>
            <form>
              <div class='form-group'>
                <label for='enewsletterEmailInput'>
                  Email Address:
                </label>
                <input class='form-control' id='enewsletterEmailInput' placeholder='' type='email'>
                <span class='help-block'>
                  Complete this form to recieve our E-newsletters and periodic emails. Cancel ar any time!
                </span>
                <a class='btn btn-primary btn-lg' href='#'>
                  Subscribe!
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>