<?php get_header(); ?>
    <?php
        $subtitle = get_post_meta($post->ID, 'project_subtitle', true);
        $location_data = get_post_meta($post->ID, 'project_location', true);
        $gallery_shortcode = get_post_meta($post->ID, 'project_gallery', true); 
        $pdf = get_post_meta($post->ID, 'project_pdf', true); 
        $cta_label = get_post_meta($post->ID, 'project_cta_label', true); 
        $cta_url = get_post_meta($post->ID, 'project_cta_url', true); 
    ?>
    <header class='header-project'>
      <div class='map-overlay'>
        <div class='inner'>
          <div class='container'>
            <div class='row'>
              <div class='col-xs-12'>
                <h1>
                  <?php the_title(); ?>
                </h1>
                <?php echo (!empty($subtitle) ? '<p class="subtitle">'.$subtitle.'</p>' : ''); ?>
                <?php echo (!empty($cta_label) ? '<a class="btn btn-primary btn-lg" href="'.$cta_url.'" target="_blank" style="margin-top: 15px;"><strong>'.$cta_label.'</strong></a>' : ''); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div id='map'></div>
    <script>
        function initMap() {
        <!-- / Styles a map in night mode. -->
        var map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: <?php echo $location_data['lat']; ?>, lng: <?php echo $location_data['lng']; ?>},
        zoom: 16,
        gestureHandling: 'none',
        scrollwheel: false,
        mapTypeControl: false,
        streetViewControl: false,
        rotateControl: false,
        zoomControl: false,
        styles: [
        {
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#222d3b'
        }
        ]
        },
        {
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#746855'
        }
        ]
        },
        {
        'elementType': 'labels.text.stroke',
        'stylers': [
        {
        'color': '#242f3e'
        }
        ]
        },
        {
        'featureType': 'administrative.locality',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#d59563'
        }
        ]
        },
        {
        'featureType': 'poi',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#d59563'
        }
        ]
        },
        {
        'featureType': 'poi.business',
        'stylers': [
        {
        'visibility': 'off'
        }
        ]
        },
        {
        'featureType': 'poi.park',
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#263d40'
        }
        ]
        },
        {
        'featureType': 'poi.park',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#5f8868'
        }
        ]
        },
        {
        'featureType': 'poi.school',
        'stylers': [
        {
        'visibility': 'off'
        }
        ]
        },
        {
        'featureType': 'road',
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#38414e'
        }
        ]
        },
        {
        'featureType': 'road',
        'elementType': 'geometry.stroke',
        'stylers': [
        {
        'color': '#212a37'
        }
        ]
        },
        {
        'featureType': 'road',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#6d737d'
        }
        ]
        },
        {
        'featureType': 'road.highway',
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#746855'
        }
        ]
        },
        {
        'featureType': 'road.highway',
        'elementType': 'geometry.stroke',
        'stylers': [
        {
        'color': '#1f2835'
        }
        ]
        },
        {
        'featureType': 'road.highway',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#f3d19c'
        }
        ]
        },
        {
        'featureType': 'transit',
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#2f3948'
        }
        ]
        },
        {
        'featureType': 'transit.station',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#d59563'
        }
        ]
        },
        {
        'featureType': 'water',
        'elementType': 'geometry',
        'stylers': [
        {
        'color': '#17263c'
        }
        ]
        },
        {
        'featureType': 'water',
        'elementType': 'labels.text.fill',
        'stylers': [
        {
        'color': '#515c6d'
        }
        ]
        },
        {
        'featureType': 'water',
        'elementType': 'labels.text.stroke',
        'stylers': [
        {
        'color': '#17263c'
        }
        ]
        }
        ]
        });
        var image = {
        url: '<?php bloginfo('template_directory') ?>/images/project-map-marker.png',
        <!-- / This marker is 20 pixels wide by 32 pixels high. -->
        size: new google.maps.Size(38, 53),
        <!-- / The origin for this image is (0, 0). -->
        origin: new google.maps.Point(0, 0),
        <!-- / The anchor for this image is the base of the flagpole at (0, 32). -->
        anchor: new google.maps.Point(19, 50)
        };
        var marker = new google.maps.Marker({
        position: {lat: <?php echo $location_data['lat']; ?>, lng: <?php echo $location_data['lng']; ?>},
        map: map,
        icon: image,
        title: '<?php the_title(); ?>'
        });
        }
      </script>
      <script async defer src='https://maps.googleapis.com/maps/api/js?key=AIzaSyD0olWGRpb866Xht9LP2G1XG2EF0FHGoBo&amp;callback=initMap'></script>
    </header>
    <div class='project-toolbar'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-sm-6 col-md-8 left'>
            <ul class='text-center project-toolbar-nav' role='tablist'><li class='active' role='presentation'><a aria-controls='information' data-toggle='tab' href='#information' role='tab'>General Information</a></li><li role='presentation'><a aria-controls='images' data-toggle='tab' href='#images' role='tab'>Image Gallery</a></li></ul>
          </div>
          <div class='col-xs-12 col-sm-6 col-md-3 col-md-offset-1 right project-dropdown'>
            <div class='inner'>
              <span class='arrow'>
                <i aria-hidden='true' class='fa fa-angle-down'></i>
              </span>
              <select class='postform' id='dynamic_select'>
                <option value=''>Jump to Project:</option>
                <?php
                    $args = array(
                      'post_type'   => 'projects',
                      'meta_query' => array (
                            array (
                                //'relation' => 'OR',
                                'key' => 'project_location', //The field to check.
                                'compare' => 'EXISTS', //Conditional statement used on the value.
                            ),  
                        ),
                    );
                    
                    $projects = get_posts($args);   
                    
                    foreach ( $projects as $post ) :
                        setup_postdata( $post ); ?>
                        <option value='<?php the_permalink(); ?>'><?php the_title(); ?></option>
                    <?php endforeach; 
                    wp_reset_postdata();     
                ?>
              </select>
              <script>
                jQuery(function(){
                  // bind change event to select
                  jQuery('#dynamic_select').on('change', function () {
                      var url = jQuery(this).val(); // get selected value
                      if (url) { // require a URL
                          window.location = url; // redirect
                      }
                      return false;
                  });
                });
            </script>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class='tab-content'>
      <div class='tab-pane active' id='information' role='tabpanel'>
        <div class='page-content right-sidebar'>
          <div class='container'>
            <div class='row'>
              <div class='col-xs-12 col-md-9 page-content-content page-content-content-lg'>
                <div class='inner'>
                  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                  
                  	<?php the_content(); ?>
                  
                  <?php endwhile; else: ?>
                  <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                  <?php endif; ?>
                </div>
              </div>
              <div class='col-xs-12 col-md-3 page-content-sidebar'>
                <?php if (!empty($pdf)) { ?>
                    <div class='sidebar-item'>
                      <a class='btn btn-lg btn-primary btn-block' href='<?php echo $pdf; ?>'>
                        <strong>
                          <i class="fa fa-file-pdf-o" aria-hidden="true"></i>&nbsp; Download Trail Map
                        </strong>
                      </a>
                    </div>
                <?php } ?>
                <?php echo landtrust_build_events_widget($post->ID); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class='tab-pane' id='images' role='tabpanel'>
        <div class='page-content full-width'>
          <div class='container'>
            <div class="row">
              <div class="col-xs-12">
                <?php echo (!empty($gallery_shortcode) ? do_shortcode($gallery_shortcode) : ''); ?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class='connect-map-cta inverted'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 text-center'>
            <img src='<?php bloginfo('template_directory') ?>/images/connect-map-cta-image.png'>
            <span>
              <strong>
                Get maps on your mobile phone
              </strong>
              with our Map App!
            </span>
            <a class='btn btn-primary btn-lg' href='#'>
              Learn More
            </a>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>