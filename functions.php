<?php
    
//======================================================================
//
// FUNCTIONS.PHP
//
// Table of Contents:
// 1. Initial Setup
// 2. Customizer Setup
// 3. Custom Post Types
// 4. Custom Taxonomies
// 5. Build Functions
//
//======================================================================


//======================================================================
// 1. INITIAL SETUP
//======================================================================


    //-----------------------------------------------------
	// Includes
	//-----------------------------------------------------
	
	if ( file_exists( dirname( __FILE__ ) . '/cmb2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/cmb2/init.php';
	} elseif ( file_exists( dirname( __FILE__ ) . '/CMB2/init.php' ) ) {
		require_once dirname( __FILE__ ) . '/CMB2/init.php';
	}
	
	
	//-----------------------------------------------------
	// Theme and Post Support
	//-----------------------------------------------------
	
	add_theme_support( 'title-tag' );			// Add theme support for the title tag
	add_theme_support( 'post-thumbnails' );		// Add theme support for post thumbnails
	
	
	//-----------------------------------------------------
	// Enable and Specify Menus
	//-----------------------------------------------------
	
	if ( function_exists( 'register_nav_menus' ) ) {
		register_nav_menus(
			array(
			  'highlighted' => 'Header - Highlighted',
			  'overlayprimary' => 'Overlay - Primary',
			  'overlaysecondary' => 'Overlay - Secondary',
			  'footerleft' => 'Footer - Left',
			  'footerright' => 'Footer - Right'
			)
		);
	}
	
	
	//-----------------------------------------------------
	// Load theme styles and scripts
	//-----------------------------------------------------
	
	function landtrust_scripts() {
		wp_enqueue_style( 'fontawesome', get_template_directory_uri().'/css/font-awesome.min.css' );
		wp_enqueue_style( 'landtrust', get_stylesheet_uri() );
		wp_enqueue_script( 'bootstrap', 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'matchHeight', get_template_directory_uri().'/js/jquery.matchHeight-min.js', array('jquery'), '1.0.0', false );
		wp_enqueue_script( 'landtrust', get_template_directory_uri().'/js/landtrust.js', array('jquery'), '1.0.0', false );
	}
	
	add_action( 'wp_enqueue_scripts', 'landtrust_scripts' );
	
	
	//-----------------------------------------------------
	// Add new image sizes
	//-----------------------------------------------------
	
	add_image_size( 'header', 1440, 500, true );
	add_image_size( 'blogfeature', 950, 530, true );
	
	update_option( 'thumbnail_size_w', 500 );
    update_option( 'thumbnail_size_h', 245 );
    update_option( 'thumbnail_crop', 1 );
    
    
    //-----------------------------------------------------
	// Build an array of Events Calendar Categories
	//-----------------------------------------------------
	
	function landtrust_build_events_cat_array() {
    	    
    	    $options = get_terms( 'tribe_events_cat', array('hide_empty' => false) );
    	    $output = '';
    	    
    	    foreach ($options as $option) {
        	    $output["{$option->term_id}"] = $option->name;
    	    }
    	    
    	    return $output;
	}
	
	
	//-----------------------------------------------------
	// Build an array of Post Categories
	//-----------------------------------------------------
	
	function landtrust_build_post_cat_array() {
    	
    	    $options = get_categories();
    	    $output = '';
    	    
    	    foreach ($options as $option) {
        	    $output["{$option->term_id}"] = $option->name;
    	    }
    	    
    	    return $output;
    	
	}
    

//======================================================================
// 2. CUSTOMIZER SETUP
//======================================================================

    /**
    * Contains methods for customizing the theme customization screen.
    * 
    * @link http://codex.wordpress.org/Theme_Customization_API
    * @since MyTheme 1.0
    */
    
    class landtrust_Customize {
        /**
        * This hooks into 'customize_register' (available as of WP 3.4) and allows
        * you to add new sections and controls to the Theme Customize screen.
        * 
        * Note: To enable instant preview, we have to actually write a bit of custom
        * javascript. See live_preview() for more.
        *  
        * @see add_action('customize_register',$func)
        * @param \WP_Customize_Manager $wp_customize
        * @link http://ottopress.com/2012/how-to-leverage-the-theme-customizer-in-your-own-themes/
        * @since MyTheme 1.0
        */
        
        public static function register ( $wp_customize ) {
            
            //1. Define a new section (if desired) to the Theme Customizer
            
            $wp_customize->add_section( 'social_options', 
                array(
                    'title' => __( 'Social Media', 'landtrust' ), //Visible title of section
                    'priority' => 35, //Determines what order this appears in
                    'capability' => 'edit_theme_options', //Capability needed to tweak
                    'description' => __('Specify social media accounts to display on the site.', 'landtrust'), //Descriptive tooltip
                ) 
            );
            
            
            //2. Register new settings to the WP database...
            
            $wp_customize->add_setting( 'social_facebook', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                array(
                    'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                    'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                    'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                ) 
            );
            
            $wp_customize->add_setting( 'social_twitter', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                array(
                    'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                    'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                    'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                ) 
            );
            
            $wp_customize->add_setting( 'social_linkedin', //No need to use a SERIALIZED name, as `theme_mod` settings already live under one db record
                array(
                    'type' => 'theme_mod', //Is this an 'option' or a 'theme_mod'?
                    'capability' => 'edit_theme_options', //Optional. Special permissions for accessing this setting.
                    'transport' => 'refresh', //What triggers a refresh of the setting? 'refresh' or 'postMessage' (instant)?
                ) 
            );
            
            //3. Finally, we define the control itself (which links a setting to a section and renders the HTML controls)...
            
            $wp_customize->add_control('landtrust_social_facebook', array(
                'label'      => __('Facebook', 'landtrust'),
                'section'    => 'social_options',
                'settings'   => 'social_facebook',
            ));
            
            $wp_customize->add_control('landtrust_social_twitter', array(
                'label'      => __('Twitter', 'landtrust'),
                'section'    => 'social_options',
                'settings'   => 'social_twitter',
            ));
            
            $wp_customize->add_control('landtrust_social_linkedin', array(
                'label'      => __('LinkedIn', 'landtrust'),
                'section'    => 'social_options',
                'settings'   => 'social_linkedin',
            ));
        }
    
        /**
        * This will output the custom WordPress settings to the live theme's WP head.
        * 
        * Used by hook: 'wp_head'
        * 
        * @see add_action('wp_head',$func)
        * @since MyTheme 1.0
        */
        
        public static function header_output() {
        ?>
            <!--Customizer CSS--> 
            <style type="text/css">
                
                
                
            </style> 
            <!--/Customizer CSS-->
        <?php
        }
       
        /**
        * This outputs the javascript needed to automate the live settings preview.
        * Also keep in mind that this function isn't necessary unless your settings 
        * are using 'transport'=>'postMessage' instead of the default 'transport'
        * => 'refresh'
        * 
        * Used by hook: 'customize_preview_init'
        * 
        * @see add_action('customize_preview_init',$func)
        * @since MyTheme 1.0
        */
        public static function live_preview() {
            wp_enqueue_script( 
                'landtrust-themecustomizer', // Give the script a unique ID
                get_template_directory_uri() . '/js/theme-customizer.js', // Define the path to the JS file
                array(  'jquery', 'customize-preview' ), // Define dependencies
                '', // Define a version (optional) 
                true // Specify whether to put in footer (leave this true)
            );
        }
    
        /**
        * This will generate a line of CSS for use in header output. If the setting
        * ($mod_name) has no defined value, the CSS will not be output.
        * 
        * @uses get_theme_mod()
        * @param string $selector CSS selector
        * @param string $style The name of the CSS *property* to modify
        * @param string $mod_name The name of the 'theme_mod' option to fetch
        * @param string $prefix Optional. Anything that needs to be output before the CSS property
        * @param string $postfix Optional. Anything that needs to be output after the CSS property
        * @param bool $echo Optional. Whether to print directly to the page (default: true).
        * @return string Returns a single line of CSS with selectors and a property.
        * @since MyTheme 1.0
        */
        public static function generate_css( $selector, $style, $mod_name, $prefix='', $postfix='', $echo=true ) {
            $return = '';
            $mod = get_theme_mod($mod_name);
            if ( ! empty( $mod ) ) {
                $return = sprintf('%s { %s:%s; }',
                $selector,
                $style,
                $prefix.$mod.$postfix
            );
                if ( $echo ) {
                    echo $return;
                }
            }
            return $return;
        }
        
        
    }
    
    // Setup the Theme Customizer settings and controls...
    add_action( 'customize_register' , array( 'landtrust_Customize' , 'register' ) );
    
    // Output custom CSS to live site
    add_action( 'wp_head' , array( 'landtrust_Customize' , 'header_output' ) );
    
    // Enqueue live preview javascript in Theme Customizer admin screen
    add_action( 'customize_preview_init' , array( 'landtrust_Customize' , 'live_preview' ) );


//======================================================================
// 5. BUILD FUNCTIONS
//======================================================================


    // Register a custom post type for Projects
    
    add_action('init', 'my_projects');
    function my_projects() 
    {
      $labels = array(
        'name' => _x('Projects', 'post type general name'),
        'singular_name' => _x('Project', 'post type singular name'),
        'add_new' => _x('Add Project', 'book'),
        'add_new_item' => __('Add New Project'),
        'edit_item' => __('Edit Project'),
        'new_item' => __('New Project'),
        'view_item' => __('View Project'),
        'search_items' => __('Search Projects'),
        'not_found' =>  __('No projects found'),
        'not_found_in_trash' => __('No projects found in Trash'), 
        'parent_item_colon' => ''
      );
      $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-location-alt',
        'hierarchical' => true,
        'menu_position' => 5,
        'supports' => array('title','thumbnail','editor','page-attributes')
      ); 
      register_post_type('projects',$args);
    }


    // Register a custom post type for Properties
    
    add_action('init', 'my_properties');
    function my_properties() 
    {
      $labels = array(
        'name' => _x('Properties', 'post type general name'),
        'singular_name' => _x('Property', 'post type singular name'),
        'add_new' => _x('Add Property', 'book'),
        'add_new_item' => __('Add New Property'),
        'edit_item' => __('Edit Property'),
        'new_item' => __('New Property'),
        'view_item' => __('View Property'),
        'search_items' => __('Search Properties'),
        'not_found' =>  __('No properties found'),
        'not_found_in_trash' => __('No properties found in Trash'), 
        'parent_item_colon' => ''
      );
      $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true, 
        'query_var' => true,
        'rewrite' => true,
        'capability_type' => 'post',
        'menu_icon' => 'dashicons-location-alt',
        'hierarchical' => true,
        'menu_position' => 5,
        'supports' => array('title','thumbnail','editor','page-attributes')
      ); 
      register_post_type('properties',$args);
    }

    

    // Register a custom post type for Projects
    
    add_action('init', 'my_blocks');
    function my_blocks() 
    {
      $labels = array(
        'name' => _x('Content Blocks', 'post type general name'),
        'singular_name' => _x('Block', 'post type singular name'),
        'add_new' => _x('Add Block', 'book'),
        'add_new_item' => __('Add New Block'),
        'edit_item' => __('Edit Block'),
        'new_item' => __('New Block'),
        'view_item' => __('View Block'),
        'search_items' => __('Search Blocks'),
        'not_found' =>  __('No blocks found'),
        'not_found_in_trash' => __('No blocks found in Trash'), 
        'parent_item_colon' => ''
      );
      $args = array(
        'labels' => $labels,
        'public' => false,
        'publicly_queryable' => false,
        'show_ui' => true, 
        'query_var' => true,
        'rewrite' => true,
        'show_in_menu' => 'edit.php?post_type=page',
        'capability_type' => 'post',
        'hierarchical' => false,
        'menu_position' => 5,
        'supports' => array('title')
      ); 
      register_post_type('blocks',$args);
    }


    //-----------------------------------------------------
	// Build featured images for the blog listing
	//-----------------------------------------------------
    
    function landtrust_build_featured_image($id) {
        
        $featured_image = get_the_post_thumbnail( $id, 'thumbnail', array( 'class' => 'img-responsive' ) );
        
        if ( empty($featured_image) ) {
            $featured_image = '<img class="img-responsive" src="http://placehold.it/500x245" width="500" height="245">' ;
        }
        
        return $featured_image;
        
    }
    
    function landtrust_build_page_header_title($id) {
        
        $page_title = get_post_meta( $id, 'page_header_title', true );
        
        if ( empty($page_title) ) {
            $page_title = get_the_title($id);
        }
        
        return $page_title;
        
    }
    
    function landtrust_build_page_header_subtitle($id) {
        
        $output = '';
        $page_subtitle = get_post_meta( $id, 'page_header_subtitle', true );
        
        if ( !empty($page_subtitle) ) {
            $output = '<p class="subtitle">'.$page_subtitle.'</p>';
        }
        
        return $output;
        
    }
    
    function landtrust_build_page_header_icon_css($id) {
        
        $output = '';
        
        $icon = get_post_meta( $id, 'page_header_icon_id', true );
        $icon2x = get_post_meta( $id, 'page_header_icon2x', true );
        
        
        if ( !empty($icon) ) {
            $output = '<style type="text/css">';
            
            
            if ( !empty($icon2x) ) {
                $icon_meta = wp_get_attachment_metadata( $icon );
                $icon = wp_get_attachment_image_url($icon, 'full');
                
                $output .= 'header.header-internal h1:before, header.header-internal h2:before {';
                $output .= 'background-image: url('.$icon.');';
                $output .= '}';
                
                $output .= '@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {';
                $output .= 'header.header-internal h1:before, header.header-internal h2:before {';
                $output .= 'background-image: url("'.$icon2x.'"); background-size: '.$icon_meta['width'].'px '.$icon_meta['height'].'px;';
                $output .= '}';
                $output .= '}';
                
            } else {
                $icon = wp_get_attachment_image_url($icon, 'full');
                $output .= 'header.header-internal h1:before, header.header-internal h2:before {';
                $output .= 'background-image: url('.$icon.') !important;';
                $output .= '}';
            }
            
            
            $output .= '</style>';
        }
        
        return $output;
        
    }
    
    function landtrust_build_front_header($id) {
        
        $output = '';
        
        // Let's start by building out the dynamic elements of the header...
        $title = get_post_meta( $id, 'front_page_header_headline', true );
        
        
        $linkblocks = '';
        $i = 1;
        
        while ($i <= 4) {
            $lb_title = get_post_meta( $id, 'front_page_header_lb'.$i.'_title', true );
            $lb_link = get_post_meta( $id, 'front_page_header_lb'.$i.'_link', true );
            $lb_icon = get_post_meta( $id, 'front_page_header_lb'.$i.'_icon_id', true );
            $lb_icon2x = get_post_meta( $id, 'front_page_header_lb'.$i.'_icon2x', true );
            
            if ( !empty($lb_icon) ) {
                $linkblocks .= '<style type="text/css">';
                
                
                if ( !empty($lb_icon2x) ) {
                    $lb_icon_meta = wp_get_attachment_metadata( $lb_icon );
                    $lb_icon = wp_get_attachment_image_url($lb_icon, 'full');
                    
                    $linkblocks .= 'a#linkblock-'.$i.':before {';
                    $linkblocks .= 'background-image: url('.$lb_icon.');';
                    $linkblocks .= '}';
                    
                    $linkblocks .= '@media only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min--moz-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (min-device-pixel-ratio: 2), only screen and (min-resolution: 192dpi), only screen and (min-resolution: 2dppx) {';
                    $linkblocks .= 'a#linkblock-'.$i.':before {';
                    $linkblocks .= 'background-image: url("'.$lb_icon2x.'"); background-size: '.$lb_icon_meta['width'].'px '.$lb_icon_meta['height'].'px;';
                    $linkblocks .= '}';
                    $linkblocks .= '}';
                    
                } else {
                    $lb_icon = wp_get_attachment_image_url($lb_icon, 'full');
                    $linkblocks .= 'a#linkblock-'.$i.':before {';
                    $linkblocks .= 'background-image: url('.$lb_icon.') !important;';
                    $linkblocks .= '}';
                }
                
                
                $linkblocks .= '</style>';
            }
            
            $linkblocks .= '
                <div class="col-xs-12 col-sm-3">
                  <a id="linkblock-'.$i.'"'.( !empty($lb_link) ? ' href="'.$lb_link.'"' : '' ).'>
                    '.( !empty($lb_title) ? $lb_title : '&nbsp;' ).'
                  </a>
                </div>
            ';
            
            
            $i++;
        }
        
        // Bring it all together
        
        $output = '
            <header id="header-home">
              <div class="shade">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 text-center">
                      <img class="img-responsive" height="210" src="'.get_bloginfo('template_directory').'/images/header-home-logo.png" width="453">
                      '.( !empty($title) ? '<h1>'.$title.'</h1>' : '').'
                      <div class="row icon-row">
                        '.$linkblocks.'
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </header>
        ';
        
        return $output;
        
    }
    
    function landtrust_build_front_about($id) {
        
        $output = '';
        
        $url = get_post_meta( $id, 'front_page_about_url', true );
        
        $output .= '
        <div id="home-mission">
          <div class="container-fluid">
            <div class="row">
              <div class="col-xs-12 text-center">
                <div class="inner">
                  <h2 class="text-hide">
                    The Land Trust leads regional and community collaborators that plan, preserve, and provide stewardship for green space in North Alabama
                  </h2>
                  <div class="learn-more text-center">
                    '.( !empty($url) ? '<a class="btn btn-lg" href="'.$url.'">Learn More</a>' : '' ).'
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        ';
        
        return $output;
        
    }
    
    function landtrust_build_front_app($id) {
        
        $content = get_post_meta( $id, 'front_page_app_content', true );
        $url = get_post_meta( $id, 'front_page_app_url', true );
        
        if (!empty($content)) {
            $content = apply_filters( 'the_content', $content );
        }
        
        $output = '
        <div id="home-app">
          <div class="hand hidden-xs">
            <img src="'.get_bloginfo("template_directory").'/images/home-app-hand.png">;</img>
          </div>
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-md-5 left">
                <div class="inner">
                  <div class="top">
                    <h2 class="text-hide">
                      Discover a New Trail
                    </h2>
                    '.( !empty($content) ? $content : '' ).'
                  </div>
                  <div class="bottom">
                    '.( !empty($url) ? '<a class="btn btn-lg" href="'.$url.'">Get The App</a>' : '' ).'
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        ';
        
        return $output;
        
    }
    
    
    function landtrust_build_front_cta($id, $key) {
        
        $cta_text = get_post_meta( $id, 'front_page_'.$key.'_text', true );
        $cta_label = get_post_meta( $id, 'front_page_'.$key.'_label', true );
        $cta_url = get_post_meta( $id, 'front_page_'.$key.'_url', true );
        
        $output = '
            <div class="green-cta">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 text-center">
                    <img src="'.get_bloginfo('template_directory').'/images/cta-icon-a.png">
                    <span>
                      '.$cta_text.'
                    </span>
                    '.( !empty($cta_url) ? '<a class="btn btn-primary btn-lg" href="'.$cta_url.'">'.$cta_label.'</a>' : '' ).'
                  </div>
                </div>
              </div>
            </div>
        ';
        
        return $output;
        
    }
    
    function landtrust_build_board_grid($id, $field) {
        
        $output = '';
        
        $members = get_post_meta( $id, 'page_'.$field.'_'.$field.'members', true );
        
        $headline = get_post_meta( $id, 'page_'.$field.'_headline', true );
        if (empty($headline)) {
            $headline = 'Members';
        }
        
        if (!empty($members)) {
            
            $output .= '
            
                <h2>
                    '.$headline.'
                </h2>
                <div class="row staff-row text-center">
            
            ';
            
                foreach ( $members as $member ) {
                    if ($member['name']) { $member_name = $member['name']; } else { $member_name = ''; }
                    if ($member['title']) { $member_title = $member['title']; } else { $member_title = ''; }
                    
                    $output .= '<div class="col-xs-12 col-sm-6 col-md-4 staff-row-item" data-mh="staff-group">';   
                    $output .= '<h4>'.$member_name.'</h4>';
                    $output .= '<span class="staff-title">'.$member_title.'</span>';
                    $output .= '</div>'; 
                }
            
            $output .= '
            
                </div>
            
            ';
            
        }
        
        return $output;
        
    }
    
    
    //-----------------------------------------------------
	// Build header image for pages
	//-----------------------------------------------------
    
    function landtrust_build_page_header_image($id) {
        
        $featured_image = get_the_post_thumbnail_url( $id, 'header');
        
        if ( empty($featured_image) ) {
            $featured_image = 'http://placehold.it/1440x500' ;
        }
        
        return ' style=" background-image: url('.$featured_image.');"';
        
    }
    
    
    function landtrust_build_connect_top_intro($id) {
        
        $output;
        
        $headline = get_post_meta($id, 'page_connect_volunteer_headline', true);
        $content = get_post_meta($id, 'page_connect_volunteer_intro', true);
        
        if (!empty($content)) {           
            $content = apply_filters('the_content',$content);
        }
        
        $output = '
            '.( !empty($headline) ? '<h2>'.$headline.'</h2>' : '').'
        ';
        
        if (!empty($content)) {
            $output .= '
                <div class="row">
                  <div class="col-xs-12 col-md-10 col-md-offset-1" id="connect-top-intro">
                    '.$content.'
                  </div>
                </div>
            ';
        }
        
        return $output;
        
    }
    
    function landtrust_build_connect_top($id) {
        
        $output = '<div class="volunteer-options">';
        
        $tabs = get_post_meta($id, 'page_connect_volunteer_tabs', true);
        
        // Build tab navigation
        
        $i = 1;
        
        $output .= '<ul class="text-center volunteer-options-nav" role="tablist">';
        
        foreach ($tabs as $tab) {
            
            $output .= '<li'.( $i == 1 ? ' class="active"' : '').' role="presentation"><a aria-controls="connectTab'.$i.'" data-toggle="tab" href="#connectTab'.$i.'" role="tab">'.$tab['tab_label'].'</a></li>';
            
            $i++;
        }
        
        $output .= '</ul>';
        
        // Build tab content panes
        
        $i = 1;
        
        $output .= '<div class="tab-content">';
        
        foreach ($tabs as $tab) {
            
            $output .= '<div class="tab-pane'.( $i == 1 ? ' active' : '' ).'" id="connectTab'.$i.'" role="tabpanel"><div class="row">';
            
            $bl = 1;
            
            while ($bl <= 4) {
                
                $content = $tab['content'.$bl];
                
                if (!empty($content)) {
                    
                    $content = apply_filters('the_content',$content);
                    $width = $tab['content'.$bl.'_width'];
                    $width = landtrust_build_connect_top_width($width);
                    
                    $output .= '<div class="'.$width.'" data-mh="connect-top-group">'.$content.'</div>';
                    
                }
                
                $bl++;
                   
            }
            
            $output .= '</div></div>';
            
            $i++;
            
        }
        
        $output .= '</div>';
        
        // Clone out the parent div
        
        $output .= '</div>';
        
        return $output;
        
    }
    
    function landtrust_build_connect_top_width($width) {
        
        switch ($width) { 
            case 'full':
                $classes = 'col-xs-12';
                break;
            case 'half':
                $classes = 'col-xs-12 col-sm-6';
                break;
            case 'third':
                $classes = 'col-xs-12 col-sm-6 col-md-4';
                break;
            default:
                $classes = 'col-xs-12';
                break;
        }
        
        return $classes;
    }
    
    
    function landtrust_build_events_widget($id) {
        
        $output = '';
        $format_type = get_post_type($id);
        
        switch ($format_type) {
            case 'page':
                $title = get_post_meta( $id, 'page_events_widget_title', true );
                $filter = get_post_meta( $id, 'page_events_widget_filter', true );
                $quantity = get_post_meta( $id, 'page_events_widget_quantity', true );
                $fallback = 'There are no scheduled events in this category at this time. Check back later for updates.';
                break;
            case 'projects':
                // Enter real project meta queries here
                $title = 'Upcoming Events';
                $filter = get_post_meta( $id, 'project_events_cal', true );;
                $quantity = '3';
                $fallback = 'There are no scheduled events for this location at this time. Check back later for updates.';
                break;
            default:
                $title = '';
                $filter = '';
                break;
        }
        
        if ( !empty($filter) ) {
			$events = tribe_get_events( array(
				'start_date'     => date( 'Y-m-d H:i:s', strtotime( 'today' ) ),
				'end_date'       => date( 'Y-m-d H:i:s', strtotime( '+1 year' ) ),
				'eventDisplay'   => 'custom',
				'posts_per_page' => $quantity,
				'tax_query' => array(
					array(
						'taxonomy' => 'tribe_events_cat',
						'field'    => 'term_id',
						'terms'    => $filter,
					),
				),
			));
		} else {
			$events = tribe_get_events( array(
				'start_date'     => date( 'Y-m-d H:i:s', strtotime( 'today' ) ),
				'end_date'       => date( 'Y-m-d H:i:s', strtotime( '+1 year' ) ),
				'eventDisplay'   => 'custom',
				'posts_per_page' => $quantity
			));
		}
		
		$output .= '
		    <div class="sidebar-item sidebar-item-events">
              <h3>
                '.( !empty($title) ? $title : "Upcoming Events" ).'
              </h3>
              <ul>
		';
		
		if ( empty( $events ) ) {
            $output .= '<li>'.$fallback.'</li>';
        } else {
            foreach( $events as $event ) {
                $output .= '
                
                    <li>
                      <h4><a href="'.get_permalink($event->ID).'">'.$event->post_title.'</a></h4>
                      <span class="date">'.tribe_get_start_date($event, false, 'F j, Y').'</span>
                    </li>
                
                ';
            }
        }
		
		
		$output .= '
		      </ul>
		    </div>
		';
		
		return $output;
    }
    
    function landtrust_build_newsroom_publications($id) {
        
        $output;
        
        $publications = get_post_meta($id, 'page_newsroom_publications_tabs', true);
        
        $output = '<ul class="newsroom-files">';
        
        foreach ($publications as $publication) {
            if (!empty($publication['file'])) {
                $output .= '<li><a href="'.$publication['file'].'" target="_blank">'.$publication['title'].'</a></li>';
            }
        }
        
        $output .= '</ul>';
        
        return $output;
        
    }
    
    
    function landtrust_build_newsroom_accountability($id) {
        
        $output;
        
        $image = get_post_meta($id, 'page_newsroom_accountability_image_id', true);
        
        if (!empty($image)) {
            $url = get_post_meta($id, 'page_newsroom_accountability_url', true);
            $output .= '
                <div class="sidebar-item sidebar-item-events">
                  <h3>
                    Accountability
                  </h3>
                  '.( !empty($url) ? '<a href="'.$url.'">' : '').wp_get_attachment_image( $image, "full", "", array( "class" => "img-responsive" ) ).( !empty($url) ? '</a>' : '').'
                </div>
            ';
        }
        
        return $output;
        
    }
    
    
    /**
     * Include metabox on front page
     * @author Ed Townend
     * @link https://github.com/WebDevStudios/CMB2/wiki/Adding-your-own-show_on-filters
     *
     * @param bool $display
     * @param array $meta_box
     * @return bool display metabox
     */
    function ed_metabox_include_front_page( $display, $meta_box ) {
        	if ( ! isset( $meta_box['show_on']['key'] ) ) {
        		return $display;
        	}
        
        	if ( 'front-page' !== $meta_box['show_on']['key'] ) {
        		return $display;
        	}
        
        	$post_id = 0;
        
        	// If we're showing it based on ID, get the current ID
        	if ( isset( $_GET['post'] ) ) {
        		$post_id = $_GET['post'];
        	} elseif ( isset( $_POST['post_ID'] ) ) {
        		$post_id = $_POST['post_ID'];
        	}
        
        	if ( ! $post_id ) {
        		return false;
        	}
        
        	// Get ID of page set as front page, 0 if there isn't one
        	$front_page = get_option( 'page_on_front' );
        
        	// there is a front page set and we're on it!
        	return $post_id == $front_page;
    }
    add_filter( 'cmb2_show_on', 'ed_metabox_include_front_page', 10, 2 );
    
    
    /**
     * Exclude metabox on front page
     */
    function ed_metabox_exclude_front_page( $display, $meta_box ) {
        	if ( ! isset( $meta_box['show_on']['key'] ) ) {
        		return $display;
        	}
        
        	if ( 'not-front-page' !== $meta_box['show_on']['key'] ) {
        		return $display;
        	}
        
        	$post_id = 0;
        
        	// If we're showing it based on ID, get the current ID
        	if ( isset( $_GET['post'] ) ) {
        		$post_id = $_GET['post'];
        	} elseif ( isset( $_POST['post_ID'] ) ) {
        		$post_id = $_POST['post_ID'];
        	}
        
        	if ( ! $post_id ) {
        		return false;
        	}
        
        	// Get ID of page set as front page, 0 if there isn't one
        	$front_page = get_option( 'page_on_front' );
        
        	// there is a front page set and we're on it!
        	return $post_id != $front_page;
    }
    add_filter( 'cmb2_show_on', 'ed_metabox_exclude_front_page', 10, 2 );
    
    
    /**
     * Include metabox on default page template
     * 'show_on' => array( 'key' => 'default-page-template', 'value' => '' ),
     */
    function ed_metabox_include_default_page( $display, $meta_box ) {
        	if ( ! isset( $meta_box['show_on']['key'] ) ) {
        		return $display;
        	}
        
        	if ( 'default-page-template' !== $meta_box['show_on']['key'] ) {
        		return $display;
        	}
        
        	$post_id = 0;
        
        	// If we're showing it based on ID, get the current ID
        	if ( isset( $_GET['post'] ) ) {
        		$post_id = $_GET['post'];
        	} elseif ( isset( $_POST['post_ID'] ) ) {
        		$post_id = $_POST['post_ID'];
        	}
        
        	if ( ! $post_id ) {
        		return false;
        	}
        
        	$page_template = get_page_template_slug( $post_id );
        	
        	if ( empty($page_template) ) {
            	$is_it_basic = true;
        	} else {
            	$is_it_basic = false;	
        	}
        
        	// there is a front page set and we're on it!
        	return $is_it_basic;
    }
    add_filter( 'cmb2_show_on', 'ed_metabox_include_default_page', 10, 2 );
    
    
    //-----------------------------------------------------
	// Metabox: Page Header Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_header_metabox' );
    
    function landtrust_register_page_header_metabox() {
	$prefix = 'page_header_';

        	$cmb_page_header = new_cmb2_box( array(
        		'id'            => $prefix . 'metabox',
        		'title'         => esc_html__( 'Page Header Options', 'landtrust' ),
        		'object_types'  => array( 'page', ), // Post type
        		'show_on' => array( 'key' => 'not-front-page', 'value' => '' ),
        	) );
        	
        	$cmb_page_header->add_field( array(
        		'name'       => esc_html__( 'Display Title', 'landtrust' ),
        		'desc'       => esc_html__( 'If you would like to display a different title other than the one entered into the Wordpress editor, enter it here.', 'landtrust' ),
        		'id'         => $prefix . 'title',
        		'type'       => 'text',
        	) );
        	
        $cmb_page_header->add_field( array(
        		'name'       => esc_html__( 'Subtitle', 'landtrust' ),
        		'desc'       => esc_html__( 'Enter a subtitle for the page here.', 'landtrust' ),
        		'id'         => $prefix . 'subtitle',
        		'type'       => 'text',
        	) );
        	
        	$cmb_page_header->add_field( array(
        		'name' => esc_html__( 'Icon', 'landtrust' ),
        		'desc' => esc_html__( 'Upload an icon or enter a URL for an icon to display in the header.', 'landtrust' ),
        		'id'   => $prefix . 'icon',
        		'type' => 'file',
        	) );
        	
        	$cmb_page_header->add_field( array(
        		'name' => esc_html__( 'Retina Icon', 'landtrust' ),
        		'desc' => esc_html__( 'Optionally, if you would like to display an icon optimized for high pixel density displays, upload it here. It should be exactly twice the resolution of the standard icon. This is not a replacement for the standard icon, which should still be uploaded.', 'landtrust' ),
        		'id'   => $prefix . 'icon2x',
        		'type' => 'file',
        	) );
        	
    }
    
    
    //-----------------------------------------------------
	// Metabox: Page Events Widget Options
	//-----------------------------------------------------
	
	add_action( 'cmb2_admin_init', 'landtrust_register_page_events_widget_metabox' );
    
    function landtrust_register_page_events_widget_metabox() {
	$prefix = 'page_events_widget_';

        	$cmb_page_events_widget = new_cmb2_box( array(
        		'id'            => $prefix . 'metabox',
        		'title'         => esc_html__( 'Page Events Widget Options', 'landtrust' ),
        		'object_types'  => array( 'page', ), // Post type
        		'show_on' => array( 'key' => 'not-front-page', 'value' => '' ),
        	) );
     
        $cmb_page_events_widget->add_field( array(
        		'name' => esc_html__( 'Widget Display', 'landtrust' ),
        		'desc' => esc_html__( 'Show the Events Widget on the side of this page.', 'landtrust' ),
        		'id'   => $prefix . 'display',
        		'type' => 'checkbox',
        	) );
        	
        	$cmb_page_events_widget->add_field( array(
        		'name'       => esc_html__( 'Title', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a custom title to display for this widget. "Upcoming Events" will be display if no custom title is entered."', 'landtrust' ),
        		'id'         => $prefix . 'title',
        		'type'       => 'text',
        	) );
        	
        	$cmb_page_events_widget->add_field( array(
        		'name'             => esc_html__( 'Number of Events', 'landtrust' ),
        		'desc'             => esc_html__( 'How many events would you like to display (if available?)', 'landtrust' ),
        		'id'               => $prefix . 'quantity',
        		'type'             => 'select',
        		'show_option_none' => false,
        		'options'          => array(
        			'1' => esc_html__( 'One', 'cmb2' ),
        			'2' => esc_html__( 'Two', 'cmb2' ),
        			'3' => esc_html__( 'Three', 'cmb2' ),
        			'4' => esc_html__( 'Four', 'cmb2' ),
        			'5' => esc_html__( 'Five', 'cmb2' ),
        			'6' => esc_html__( 'Six', 'cmb2' ),
        		),
        	) );
        	
        	$cmb_page_events_widget->add_field( array(
        		'name'             => esc_html__( 'Category Filter', 'landtrust' ),
        		'desc'             => esc_html__( 'If you would like to limit events to a specific category, select that category here.', 'landtrust' ),
        		'id'               => $prefix . 'filter',
        		'type'             => 'select',
        		'show_option_none' => true,
        		'options'          => landtrust_build_events_cat_array(),
        	) );
        	
    }
    
    
    //-----------------------------------------------------
	// Metabox: Page Blog Widget Options
	//-----------------------------------------------------
	
	add_action( 'cmb2_admin_init', 'landtrust_register_page_blog_widget_metabox' );
    
    function landtrust_register_page_blog_widget_metabox() {
	$prefix = 'page_blog_widget_';

        	$cmb_page_blog_widget = new_cmb2_box( array(
        		'id'            => $prefix . 'metabox',
        		'title'         => esc_html__( 'Page Blog Widget Options', 'landtrust' ),
        		'object_types'  => array( 'page', ), // Post type
        		'show_on' => array( 'key' => 'not-front-page', 'value' => '' ),
        	) );
     
        $cmb_page_blog_widget->add_field( array(
        		'name' => esc_html__( 'Widget Display', 'landtrust' ),
        		'desc' => esc_html__( 'Show the Blog Widget on the side of this page.', 'landtrust' ),
        		'id'   => $prefix . 'display',
        		'type' => 'checkbox',
        	) );
        	
        	$cmb_page_blog_widget->add_field( array(
        		'name'             => esc_html__( 'Category Filter', 'landtrust' ),
        		'desc'             => esc_html__( 'If you would like to limit events to a specific category, select that category here.', 'landtrust' ),
        		'id'               => $prefix . 'filter',
        		'type'             => 'select',
        		'show_option_none' => true,
        		'options'          => landtrust_build_post_cat_array(),
        	) );
        	
    }
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_board_metabox' );
    /**
     * Hook in and add a metabox to demonstrate repeatable grouped fields
     */
    function landtrust_register_page_board_metabox() {
        	$prefix = 'page_board_';
        
        	/**
        	 * Repeatable Field Groups
        	 */
        	$cmb_board_group = new_cmb2_box( array(
        		'id'           => $prefix . 'metabox',
        		'title'        => esc_html__( 'Board Members', 'landtrust' ),
        		'object_types' => array( 'page', ),
        		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-staff.php' ),
        	) );
        	
        $cmb_board_group->add_field( array(
        		'name'       => esc_html__( 'Section Headline', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a title for the Board section of the page.', 'landtrust' ),
        		'id'         => $prefix . 'headline',
        		'type'       => 'text',
        	) );
        
        	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
        	$group_field_id = $cmb_board_group->add_field( array(
        		'id'          => $prefix . 'boardmembers',
        		'type'        => 'group',
        		'description' => esc_html__( 'Generates board members', 'landtrust' ),
        		'options'     => array(
        			'group_title'   => esc_html__( 'Board Member {#}', 'landtrust' ), // {#} gets replaced by row number
        			'add_button'    => esc_html__( 'Add Another Board Member', 'landtrust' ),
        			'remove_button' => esc_html__( 'Remove Board Member', 'landtrust' ),
        			'sortable'      => true, // beta
        			// 'closed'     => true, // true to have the groups closed by default
        		),
        	) );
        
        	/**
        	 * Group fields works the same, except ids only need
        	 * to be unique to the group. Prefix is not needed.
        	 *
        	 * The parent field's id needs to be passed as the first argument.
        	 */
        	$cmb_board_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Name', 'landtrust' ),
        		'id'         => 'name',
        		'type'       => 'text',
        	) );
        	
        $cmb_board_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Title', 'landtrust' ),
        		'id'         => 'title',
        		'type'       => 'text',
        	) );
    }
    
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_staff_metabox' );
    /**
     * Hook in and add a metabox to demonstrate repeatable grouped fields
     */
    function landtrust_register_page_staff_metabox() {
        	$prefix = 'page_staff_';
        
        	/**
        	 * Repeatable Field Groups
        	 */
        	$cmb_staff_group = new_cmb2_box( array(
        		'id'           => $prefix . 'metabox',
        		'title'        => esc_html__( 'Staff Members', 'landtrust' ),
        		'object_types' => array( 'page', ),
        		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-staff.php' ),
        	) );
        	
        $cmb_staff_group->add_field( array(
        		'name'       => esc_html__( 'Section Headline', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a title for the Staff section of the page.', 'landtrust' ),
        		'id'         => $prefix . 'headline',
        		'type'       => 'text',
        	) );
        
        	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
        	$group_field_id = $cmb_staff_group->add_field( array(
        		'id'          => $prefix . 'staffmembers',
        		'type'        => 'group',
        		'description' => esc_html__( 'Generates staff members', 'landtrust' ),
        		'options'     => array(
        			'group_title'   => esc_html__( 'Staff Member {#}', 'landtrust' ), // {#} gets replaced by row number
        			'add_button'    => esc_html__( 'Add Another Staff Member', 'landtrust' ),
        			'remove_button' => esc_html__( 'Remove Staff Member', 'landtrust' ),
        			'sortable'      => true, // beta
        			// 'closed'     => true, // true to have the groups closed by default
        		),
        	) );
        
        	/**
        	 * Group fields works the same, except ids only need
        	 * to be unique to the group. Prefix is not needed.
        	 *
        	 * The parent field's id needs to be passed as the first argument.
        	 */
        	$cmb_staff_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Name', 'landtrust' ),
        		'id'         => 'name',
        		'type'       => 'text',
        	) );
        	
        $cmb_staff_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Title', 'landtrust' ),
        		'id'         => 'title',
        		'type'       => 'text',
        	) );
    }
    
    
    //-----------------------------------------------------
	// Metabox: Front Page Header Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_front_page_header_metabox' );
    
    function landtrust_register_front_page_header_metabox() {
	$prefix = 'front_page_header_';

    	$cmb_front_page_header = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Page Header Options', 'landtrust' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'       => esc_html__( 'Display Title', 'landtrust' ),
    		'desc'       => esc_html__( 'If you would like to display a different title other than the one entered into the Wordpress editor, enter it here.', 'landtrust' ),
    		'id'         => $prefix . 'headline',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'     => esc_html__( 'Link Block 1', 'cmb2' ),
    		'desc'     => esc_html__( 'Specify an image, title, and URL for the first link block.', 'cmb2' ),
    		'id'       => $prefix . 'lb1_info',
    		'type'     => 'title',
    		'on_front' => false,
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Upload an icon or enter a URL for an icon to display for the first link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb1_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Retina Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Optionally, if you would like to display an icon optimized for high pixel density displays, upload it here. It should be exactly twice the resolution of the standard icon. This is not a replacement for the standard icon, which should still be uploaded.', 'landtrust' ),
    		'id'   => $prefix . 'lb1_icon2x',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'       => esc_html__( 'Link Title', 'landtrust' ),
    		'desc'       => esc_html__( 'Specify a title for this link block.', 'landtrust' ),
    		'id'         => $prefix . 'lb1_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Link URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb1_link',
    		'type' => 'text_url',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'     => esc_html__( 'Link Block 2', 'cmb2' ),
    		'desc'     => esc_html__( 'Specify an image, title, and URL for the second link block.', 'cmb2' ),
    		'id'       => $prefix . 'lb2_info',
    		'type'     => 'title',
    		'on_front' => false,
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Upload an icon or enter a URL for an icon to display for the first link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb2_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Retina Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Optionally, if you would like to display an icon optimized for high pixel density displays, upload it here. It should be exactly twice the resolution of the standard icon. This is not a replacement for the standard icon, which should still be uploaded.', 'landtrust' ),
    		'id'   => $prefix . 'lb2_icon2x',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'       => esc_html__( 'Link Title', 'landtrust' ),
    		'desc'       => esc_html__( 'Specify a title for this link block.', 'landtrust' ),
    		'id'         => $prefix . 'lb2_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Link URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb2_link',
    		'type' => 'text_url',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'     => esc_html__( 'Link Block 3', 'cmb2' ),
    		'desc'     => esc_html__( 'Specify an image, title, and URL for the third link block.', 'cmb2' ),
    		'id'       => $prefix . 'lb3_info',
    		'type'     => 'title',
    		'on_front' => false,
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Upload an icon or enter a URL for an icon to display for the first link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb3_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Retina Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Optionally, if you would like to display an icon optimized for high pixel density displays, upload it here. It should be exactly twice the resolution of the standard icon. This is not a replacement for the standard icon, which should still be uploaded.', 'landtrust' ),
    		'id'   => $prefix . 'lb3_icon2x',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'       => esc_html__( 'Link Title', 'landtrust' ),
    		'desc'       => esc_html__( 'Specify a title for this link block.', 'landtrust' ),
    		'id'         => $prefix . 'lb3_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Link URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb3_link',
    		'type' => 'text_url',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'     => esc_html__( 'Link Block 4', 'cmb2' ),
    		'desc'     => esc_html__( 'Specify an image, title, and URL for the fourth link block.', 'cmb2' ),
    		'id'       => $prefix . 'lb4_info',
    		'type'     => 'title',
    		'on_front' => false,
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Upload an icon or enter a URL for an icon to display for the first link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb4_icon',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Retina Icon', 'landtrust' ),
    		'desc' => esc_html__( 'Optionally, if you would like to display an icon optimized for high pixel density displays, upload it here. It should be exactly twice the resolution of the standard icon. This is not a replacement for the standard icon, which should still be uploaded.', 'landtrust' ),
    		'id'   => $prefix . 'lb4_icon2x',
    		'type' => 'file',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name'       => esc_html__( 'Link Title', 'landtrust' ),
    		'desc'       => esc_html__( 'Specify a title for this link block.', 'landtrust' ),
    		'id'         => $prefix . 'lb4_title',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_header->add_field( array(
    		'name' => esc_html__( 'Link URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'lb4_link',
    		'type' => 'text_url',
    	) );
    	  	
    }
    
    
    //-----------------------------------------------------
	// Metabox: Front Page CTA 1 Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_front_page_cta1_metabox' );
    
    function landtrust_register_front_page_cta1_metabox() {
	$prefix = 'front_page_cta1_';

    	$cmb_front_page_cta1 = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Call to Action 1 Options', 'landtrust' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    	) );
    	
    	$cmb_front_page_cta1->add_field( array(
    		'name'       => esc_html__( 'CTA Text', 'landtrust' ),
    		'desc'       => esc_html__( 'Enter a short description to display on this CTA.', 'landtrust' ),
    		'id'         => $prefix . 'text',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_cta1->add_field( array(
    		'name'       => esc_html__( 'Button Label', 'landtrust' ),
    		'desc'       => esc_html__( 'Enter a label for the CTA\'s button.', 'landtrust' ),
    		'id'         => $prefix . 'label',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_cta1->add_field( array(
    		'name' => esc_html__( 'Button URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'url',
    		'type' => 'text_url',
    	) );
    	
    }
    
    
    //-----------------------------------------------------
	// Metabox: Front Page About Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_front_page_about_metabox' );
    
    function landtrust_register_front_page_about_metabox() {
	$prefix = 'front_page_about_';

    	$cmb_front_page_about = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'About Options', 'landtrust' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    	) );
    	
    	$cmb_front_page_about->add_field( array(
    		'name' => esc_html__( 'About Page URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify the link to the About Us page.', 'landtrust' ),
    		'id'   => $prefix . 'url',
    		'type' => 'text_url',
    	) );
    	
    }
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_front_page_cta2_metabox' );
    
    function landtrust_register_front_page_cta2_metabox() {
	$prefix = 'front_page_cta2_';

    	$cmb_front_page_cta2 = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Call to Action 2 Options', 'landtrust' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    	) );
    	
    	$cmb_front_page_cta2->add_field( array(
    		'name'       => esc_html__( 'CTA Text', 'landtrust' ),
    		'desc'       => esc_html__( 'Enter a short description to display on this CTA.', 'landtrust' ),
    		'id'         => $prefix . 'text',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_cta2->add_field( array(
    		'name'       => esc_html__( 'Button Label', 'landtrust' ),
    		'desc'       => esc_html__( 'Enter a label for the CTA\'s button.', 'landtrust' ),
    		'id'         => $prefix . 'label',
    		'type'       => 'text',
    	) );
    	
    	$cmb_front_page_cta2->add_field( array(
    		'name' => esc_html__( 'Button URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify a URL for the link block.', 'landtrust' ),
    		'id'   => $prefix . 'url',
    		'type' => 'text_url',
    	) );
    	
    }
    
    
    //-----------------------------------------------------
	// Metabox: Front Page App Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_front_page_app_metabox' );
    
    function landtrust_register_front_page_app_metabox() {
	$prefix = 'front_page_app_';
	
	    $cmb_front_page_app = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'App Section Options', 'landtrust' ),
    		'object_types'  => array( 'page', ), // Post type
    		'show_on' => array( 'key' => 'front-page', 'value' => '' ),
    	) );
	
        $cmb_front_page_app->add_field( array(
    		'name' => esc_html__( 'Section Content', 'landtrust' ),
    		'desc' => esc_html__( 'Specify content for this section.', 'landtrust' ),
    		'id'   => $prefix . 'content',
    		'type' => 'wysiwyg',
    	) );
	
	    $cmb_front_page_app->add_field( array(
    		'name' => esc_html__( 'Mobile App Page URL', 'landtrust' ),
    		'desc' => esc_html__( 'Specify the link to the About Us page.', 'landtrust' ),
    		'id'   => $prefix . 'url',
    		'type' => 'text_url',
    	) );
	
	}
	
	
	//-----------------------------------------------------
	// Metabox: Project Options
	//-----------------------------------------------------
    
    add_action( 'cmb2_admin_init', 'landtrust_register_project_metabox' );
    
    function landtrust_register_project_metabox() {
	$prefix = 'project_';

    	$cmb_project = new_cmb2_box( array(
    		'id'            => $prefix . 'metabox',
    		'title'         => esc_html__( 'Property Options', 'landtrust' ),
    		'object_types'  => array( 'properties', ), // Post type
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'       => esc_html__( 'Subtitle', 'landtrust' ),
    		'desc'       => esc_html__( 'Enter a subtitle to use on this project\'s page.', 'landtrust' ),
    		'id'         => $prefix . 'subtitle',
    		'type'       => 'text',
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'       => esc_html__( 'Call to Action Label', 'landtrust' ),
    		'desc'       => esc_html__( 'If you would like to include a Call to Action button in the header of this project\'s page, give it a label here.', 'landtrust' ),
    		'id'         => $prefix . 'cta_label',
    		'type'       => 'text',
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'       => esc_html__( 'Call to Action URL', 'landtrust' ),
    		'desc'       => esc_html__( 'If including a CTA button, specify a URL to link to here.', 'landtrust' ),
    		'id'         => $prefix . 'cta_url',
    		'type'       => 'text_url',
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'       => esc_html__( 'Location', 'landtrust' ),
    		'desc'       => esc_html__( 'Select the location of this project\'s trailhead on the map.', 'landtrust' ),
    		'id'         => $prefix . 'location',
    		'limit_drawing' => true,
    		'type'       => 'snapmap',
    	) );
    	
    	$cmb_project->add_field( array(
    		'name' => esc_html__( 'Trail Map PDF', 'landtrust' ),
    		'desc' => esc_html__( 'Upload a PDF containing a map of this project\'s trails.', 'landtrust' ),
    		'id'   => $prefix . 'pdf',
    		'type' => 'file',
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'             => esc_html__( 'Events Calendar Category', 'landtrust' ),
    		'desc'             => esc_html__( 'To show a list of upcoming events for this project, select its corresponding event category.', 'landtrust' ),
    		'id'               => $prefix . 'events_cat',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => landtrust_build_events_cat_array(),
    	) );
    	
    	$cmb_project->add_field( array(
    		'name'             => esc_html__( 'Blog Category', 'landtrust' ),
    		'desc'             => esc_html__( 'To show a list of recent blog posts about this project, select its corresponding blog category.', 'landtrust' ),
    		'id'               => $prefix . 'blog_cat',
    		'type'             => 'select',
    		'show_option_none' => true,
    		'options'          => landtrust_build_post_cat_array(),
    	) );
    	
    	$cmb_project->add_field( array(
    		'name' => esc_html__( 'Image Gallery Shortcode', 'landtrust' ),
    		'desc' => esc_html__( 'To display an image gallery for this project, create a galley using the gallery plugin of your choice and paste the shortcode provided by that shortcode here.', 'landtrust' ),
    		'id'   => $prefix . 'gallery',
    		'type' => 'text',
    	) );
    }
    
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_connect_volunteer_metabox' );
    /**
     * Hook in and add a metabox to demonstrate repeatable grouped fields
     */
    function landtrust_register_page_connect_volunteer_metabox() {
        	$prefix = 'page_connect_volunteer_';
        
        	/**
        	 * Repeatable Field Groups
        	 */
        	$cmb_connect_volunteer_group = new_cmb2_box( array(
        		'id'           => $prefix . 'metabox',
        		'title'        => esc_html__( 'Volunteer Section', 'landtrust' ),
        		'object_types' => array( 'page', ),
        		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-connect.php' ),
        	) );
        	
            $cmb_connect_volunteer_group->add_field( array(
        		'name'       => esc_html__( 'Section Headline', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a title for the Volunteer section of the page.', 'landtrust' ),
        		'id'         => $prefix . 'headline',
        		'type'       => 'text',
        	) );
        	
        	$cmb_connect_volunteer_group->add_field( array(
        		'name'    => 'Intro Content',
            	'id'      => $prefix . 'intro',
            	'type'    => 'wysiwyg',
        		'options' => array(
            		'media_buttons' => false, // show insert/upload button(s)
            	    'textarea_rows' => 8, // rows="..."
            	    'teeny' => false, // output the minimal editor config used in Press This
            	),
        	) );
        
        	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
        	$group_field_id = $cmb_connect_volunteer_group->add_field( array(
        		'id'          => $prefix . 'tabs',
        		'type'        => 'group',
        		'description' => esc_html__( 'Generates tabs for the volunteer section.', 'landtrust' ),
        		'options'     => array(
        			'group_title'   => esc_html__( 'Tab {#}', 'landtrust' ), // {#} gets replaced by row number
        			'add_button'    => esc_html__( 'Add Another Tab', 'landtrust' ),
        			'remove_button' => esc_html__( 'Remove Tab', 'landtrust' ),
        			'sortable'      => true, // beta
        			// 'closed'     => true, // true to have the groups closed by default
        		),
        	) );
        
        	/**
        	 * Group fields works the same, except ids only need
        	 * to be unique to the group. Prefix is not needed.
        	 *
        	 * The parent field's id needs to be passed as the first argument.
        	 */
        	/* $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        			'name'             => 'Content Blocks',
                	'desc'             => 'Add pre-built content blocks to this tab. Content blocks can be created <a href="/wp-admin/edit.php?post_type=blocks">here</a>.',
                	'id'               => 'blocks',
                	'type'             => 'select',
                	'show_option_none' => true,
                	'repeatable' => true,
                	'options'          => array(
                		'standard' => __( 'Option One', 'cmb2' ),
                		'custom'   => __( 'Option Two', 'cmb2' ),
                		'none'     => __( 'Option Three', 'cmb2' ),
                	),
        	) ); */
        	
        	$cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Tab Title', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a name for this tab.', 'landtrust' ),
        		'id'         => 'tab_label',
        		'type'       => 'text',
            ) );
        	
        	$cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        		'name'    => 'Content Block 1',
            	'id'      => 'content1',
            	'type'    => 'wysiwyg',
            		'options' => array(
                		'media_buttons' => false, // show insert/upload button(s)
                	    'textarea_rows' => 5, // rows="..."
                	    'teeny' => false, // output the minimal editor config used in Press This
                	),
            	
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
            	'name'    => 'Block Width',
            	'id'      => 'content1_width',
            	'type'    => 'radio_inline',
            	'options' => array(
            		'full' => __( '100%', 'cmb2' ),
            		'half'   => __( '50%', 'cmb2' ),
            		'third'     => __( '33%', 'cmb2' ),
            	),
            	'default' => 'full',
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        		'name'    => 'Content Block 2',
            	'id'      => 'content2',
            	'type'    => 'wysiwyg',
            		'options' => array(
                		'media_buttons' => false, // show insert/upload button(s)
                	    'textarea_rows' => 5, // rows="..."
                	    'teeny' => false, // output the minimal editor config used in Press This
                	),
            	
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
            	'name'    => 'Block Width',
            	'id'      => 'content2_width',
            	'type'    => 'radio_inline',
            	'options' => array(
            		'full' => __( '100%', 'cmb2' ),
            		'half'   => __( '50%', 'cmb2' ),
            		'third'     => __( '33%', 'cmb2' ),
            	),
            	'default' => 'full',
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        		'name'    => 'Content Block 3',
            	'id'      => 'content3',
            	'repeatable' => true,
            	'type'    => 'wysiwyg',
            		'options' => array(
                		'media_buttons' => false, // show insert/upload button(s)
                	    'textarea_rows' => 5, // rows="..."
                	    'teeny' => false, // output the minimal editor config used in Press This
                	),
            	
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
            	'name'    => 'Block Width',
            	'id'      => 'content3_width',
            	'type'    => 'radio_inline',
            	'options' => array(
            		'full' => __( '100%', 'cmb2' ),
            		'half'   => __( '50%', 'cmb2' ),
            		'third'     => __( '33%', 'cmb2' ),
            	),
            	'default' => 'full',
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
        		'name'    => 'Content Block 4',
            	'id'      => 'content4',
            	'repeatable' => true,
            	'type'    => 'wysiwyg',
            		'options' => array(
                		'media_buttons' => false, // show insert/upload button(s)
                	    'textarea_rows' => 5, // rows="..."
                	    'teeny' => false, // output the minimal editor config used in Press This
                	),
            	
            ) );
            
            $cmb_connect_volunteer_group->add_group_field( $group_field_id, array(
            	'name'    => 'Block Width',
            	'id'      => 'content4_width',
            	'type'    => 'radio_inline',
            	'options' => array(
            		'full' => __( '100%', 'cmb2' ),
            		'half'   => __( '50%', 'cmb2' ),
            		'third'     => __( '33%', 'cmb2' ),
            	),
            	'default' => 'full',
            ) );
    }
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_newsroom_publications_metabox' );
    /**
     * Hook in and add a metabox to demonstrate repeatable grouped fields
     */
    function landtrust_register_page_newsroom_publications_metabox() {
        	$prefix = 'page_newsroom_publications_';
        
        	/**
        	 * Repeatable Field Groups
        	 */
        	$cmb_newsroom_publications_group = new_cmb2_box( array(
        		'id'           => $prefix . 'metabox',
        		'title'        => esc_html__( 'Publications', 'landtrust' ),
        		'object_types' => array( 'page', ),
        		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-newsroom.php' ),
        	) );
        
        	// $group_field_id is the field id string, so in this case: $prefix . 'demo'
        	$group_field_id = $cmb_newsroom_publications_group->add_field( array(
        		'id'          => $prefix . 'tabs',
        		'type'        => 'group',
        		'description' => esc_html__( 'Add publications to display on the newsroom page.', 'landtrust' ),
        		'options'     => array(
        			'group_title'   => esc_html__( 'Publication {#}', 'landtrust' ), // {#} gets replaced by row number
        			'add_button'    => esc_html__( 'Add Another Publication', 'landtrust' ),
        			'remove_button' => esc_html__( 'Remove Publication', 'landtrust' ),
        			'sortable'      => true, // beta
        			// 'closed'     => true, // true to have the groups closed by default
        		),
        	) );
        	
        	$cmb_newsroom_publications_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'Title', 'landtrust' ),
        		'desc'       => esc_html__( 'Specify a name for this publication.', 'landtrust' ),
        		'id'         => 'title',
        		'type'       => 'text',
            ) );
            
            $cmb_newsroom_publications_group->add_group_field( $group_field_id, array(
        		'name'       => esc_html__( 'File', 'landtrust' ),
        		'desc'       => esc_html__( 'Upload or pick a file for this publication.', 'landtrust' ),
        		'id'         => 'file',
        		'type'       => 'file',
            ) );       
    }
    
    
    add_action( 'cmb2_admin_init', 'landtrust_register_page_newsroom_accountability_metabox' );
    /**
     * Hook in and add a metabox to demonstrate repeatable grouped fields
     */
    function landtrust_register_page_newsroom_accountability_metabox() {
        	$prefix = 'page_newsroom_accountability_';
        
        	/**
        	 * Repeatable Field Groups
        	 */
        	$cmb_newsroom_accountability_group = new_cmb2_box( array(
        		'id'           => $prefix . 'metabox',
        		'title'        => esc_html__( 'Accountability', 'landtrust' ),
        		'object_types' => array( 'page', ),
        		'show_on'      => array( 'key' => 'page-template', 'value' => 'page-template-newsroom.php' ),
        	) );
        	
        	$cmb_newsroom_accountability_group->add_field( array(
        		'name' => esc_html__( 'Image', 'landtrust' ),
        		'desc' => esc_html__( 'Pick an image to display in the Accountability sidebar box.', 'landtrust' ),
        		'id'   => $prefix . 'image',
        		'type' => 'file',
        	) );
        	
        	$cmb_newsroom_accountability_group->add_field( array(
        		'name' => esc_html__( 'Accountability Page URL', 'landtrust' ),
        		'desc' => esc_html__( 'Specify a URL to link the image to.', 'landtrust' ),
        		'id'   => $prefix . 'url',
        		'type' => 'text_url',
        	) );
    
    }