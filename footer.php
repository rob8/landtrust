    <footer>
      <div class='container'>
        <div class='row footer-nav-row'>
          <div class='col-xs-12 col-md-5 footer-nav footer-nav-left'>
            <div class='footer-nav-inner' data-mh='footer-nav-group'>
              <?php
                wp_nav_menu( array('theme_location' => 'footerleft', 'depth' => 1, 'fallback_cb' => false, 'container' => false )); 
              ?>
            </div>
          </div>
          <div class='col-xs-12 col-md-2 footer-nav-logo hidden-xs hidden-sm' data-mh='footer-nav-group'>
            <img class='img-responsive' src='<?php bloginfo('template_directory') ?>/images/footer-logo.png'>
          </div>
          <div class='col-xs-12 col-md-5 footer-nav footer-nav-right'>
            <div class='footer-nav-inner' data-mh='footer-nav-group'>
              <?php
                wp_nav_menu( array('theme_location' => 'footerright', 'depth' => 1, 'fallback_cb' => false, 'container' => false )); 
              ?>
            </div>
          </div>
        </div>
        <div class='row footer-social-row'>
          <div class='col-xs-12 text-center footer-social'>
            <ul>
              <li>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-twitter fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='hidden'>
                    Twitter
                  </span>
                </a>
              </li>
              <li>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-facebook fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='hidden'>
                    Facebook
                  </span>
                </a>
              </li>
              <li>
                <a href='#'>
                  <span aria-hidden='true' class='fa-stack fa-lg'>
                    <i class='fa fa-circle fa-stack-2x'></i>
                    <i class='fa fa-instagram fa-stack-1x fa-inverse'></i>
                  </span>
                  <span class='hidden'>
                    Instagram
                  </span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <div class='row footer-copyright-row'>
          <div class='col-xs-12 text-center footer-copyright'>
            <p>
              Copyright 2017 Land Trust of North Alabama
            </p>
          </div>
        </div>
        <div class='row footer-tag-row'>
          <div class='col-xs-12 text-center footer-tag'>
            <a href='https://pixelwatt.com' id='pixelwatt-tag' target='_blank'>
              Site by PixelWatt
            </a>
          </div>
        </div>
      </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>