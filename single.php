<?php get_header(); ?>
    <header class='header-internal header-internal-blog'>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <h2>The Land Trust Blog</h2>
              <p class='subtitle'>
                Stay Up To Date
              </p>
            </div>
          </div>
        </div>
      </div>
    </header>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
    <div class='page-content full-width blog-post'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-md-10 col-md-offset-1'>
            <h1><?php the_title(); ?></h1>
            <div class='row blog-post-meta'>
              <div class='col-xs-12 col-sm-6 col-md-4 blog-post-meta-author'>
                <?php echo get_avatar( get_the_author_meta( 'ID' ), 36 ); ?> By <strong><?php the_author(); ?></strong>
              </div>
              <div class='col-xs-12 col-sm-6 col-md-4 blog-post-meta-date'>
                <img class='avatar' height='36' src='<?php bloginfo('template_directory') ?>/images/blog-post-meta-date.png' width='25'> <?php the_time('F j, Y'); ?>
              </div>
            </div>
            <div class='blog-post-inner'>
              <?php the_post_thumbnail('blogfeature', ['class' => 'img-responsive blog-post-featured', 'title' => 'Feature image']); ?>
              <?php the_content(); ?>
            </div>
          </div>
        </div>
      </div>
    </div>
    <?php endwhile; else: endif; ?>
<?php get_footer(); ?>
