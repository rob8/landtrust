<?php get_header(); ?>
    <header class='header-internal header-internal-blog'>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <h1>The Land Trust Blog</h1>
              <p class='subtitle'>
                Stay Up To Date
              </p>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='post-grid'>
      <div class='container'>
        <div class='row'>
          <div class='col-xs-12 col-md-10 col-md-offset-1'>
            <div class='row'>
                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                  <div class='col-xs-12 col-sm-6 post-grid-item'>
                    <div class='thumbnail'>
                      <a href='<?php the_permalink(); ?>'>
                        <?php echo landtrust_build_featured_image($post->ID); ?>
                      </a>
                      <div class='caption'>
                        <div class='caption-top' data-mh='grid-item-group'>
                          <h3>
                            <a href='<?php the_permalink(); ?>'>
                              <?php the_title(); ?>
                            </a>
                          </h3>
                          <p class='post-date'>
                            <?php the_time('F j, Y'); ?>
                          </p>
                        </div>
                        <div class='caption-bottom'>
                          <div class='row'>
                            <div class='col-xs-6 text-left'>
                              &nbsp;
                            </div>
                            <div class='col-xs-6 text-right'>
                              <a class='btn btn-primary' href='<?php the_permalink(); ?>'>
                                Read More
                              </a>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php endwhile; else: endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
<?php get_footer(); ?>
