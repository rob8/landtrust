<?php

    //-----------------------------------------------------
	// Load Widget Inclusions
	//-----------------------------------------------------
	
	$events_display = get_post_meta( $post->ID, 'page_events_widget_display', true );
	$blog_display = get_post_meta( $post->ID, 'page_blog_widget_display', true );
	
	if ( ($events_display == 'on') || ($blog_display == 'on') ) {
    	    $sidebar_display = TRUE;
	} else {
    	    $sidebar_display = FALSE;
	}
    
?>

<?php get_header(); ?>
    <header class='header-internal'<?php echo landtrust_build_page_header_image($post->ID); ?>>
      <div class='shade'>
        <div class='container'>
          <div class='row'>
            <div class='col-xs-12 col-sm-10 col-sm-offset-1'>
              <?php echo landtrust_build_page_header_icon_css($post->ID); ?>
              <h1><?php echo landtrust_build_page_header_title($post->ID); ?></h1>
              <?php echo landtrust_build_page_header_subtitle($post->ID); ?>
            </div>
          </div>
        </div>
      </div>
    </header>
    <div class='page-content<?php echo ( $sidebar_display == true ? ' right-sidebar' : ' full-width' ); ?>'>
      <div class='container'>
        <div class='row'>
          <div class='<?php echo ( $sidebar_display == true ? 'col-xs-12 col-md-7 col-md-offset-1 page-content-content' : 'col-xs-12 col-md-10 col-md-offset-1' ); ?>'>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
              <?php the_content(); ?>
            <?php endwhile; else: endif; ?>
          </div>
          <?php if ($sidebar_display == true) { ?>
          <div class='col-xs-12 col-md-3 page-content-sidebar'>
            <?php if ($events_display == 'on') { ?>
            <?php echo landtrust_build_events_widget($post->ID); ?>
            <?php } ?>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>
<?php get_footer(); ?>