<?php get_header(); ?>
    
    <?php echo landtrust_build_front_header($post->ID); ?>
    <?php echo landtrust_build_front_cta($post->ID,'cta1'); ?>
    <?php echo landtrust_build_front_about($post->ID); ?>
    <?php echo landtrust_build_front_cta($post->ID,'cta2'); ?>
    <?php echo landtrust_build_front_app($post->ID); ?>

<?php get_footer(); ?>